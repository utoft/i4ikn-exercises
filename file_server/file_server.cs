using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace tcp
{
	public class file_server
	{
		/// <summary>
		/// The PORT
		/// </summary>
		const int PORT = 9000;
		/// <summary>
		/// The BUFSIZE
		/// </summary>
		const int BUFSIZE = 1000;

		/// <summary>
		/// Initializes a new instance of the <see cref="file_server"/> class.
		/// Opretter en socket.
		/// Venter på en connect fra en klient.
		/// Modtager filnavn
		/// Finder filstørrelsen
		/// Kalder metoden sendFile
		/// Lukker socketen og programmet
		/// </summary>
		public file_server ()
		{
			TcpListener listener = new TcpListener (IPAddress.Any, PORT);
			listener.Start ();
			Console.WriteLine ("Server: Listening for connections on {0}...", listener.LocalEndpoint);

			//while (true) 
			//{
			TcpClient client = listener.AcceptTcpClient ();


			NetworkStream receive = client.GetStream ();
			Console.WriteLine ("Server: Recieved connection from {0}", client.Client.RemoteEndPoint);
			var fileName = LIB.readTextTCP (receive);
			Console.WriteLine ("Server: Requested file is {0}", fileName);
		

			var fileSize = LIB.check_File_Exists (fileName); //0 if not found

			//Send string of the filename
			LIB.writeTextTCP (receive, Convert.ToString (fileSize));
			Console.WriteLine ("Server: Sending file size of {0} bytes", fileSize);


			if (fileSize > 0) {
				Console.WriteLine ("Server: Sending file...");
				this.sendFile (fileName, fileSize, receive); //Send file
				Console.WriteLine ("Server: Sending file done");
			}
			//}

			listener.Stop ();
			Console.WriteLine ("Server: Shutting down. Bye :)");

		}

		/// <summary>
		/// Sends the file.
		/// </summary>
		/// <param name='fileName'>
		/// The filename.
		/// </param>
		/// <param name='fileSize'>
		/// The filesize.
		/// </param>
		/// <param name='io'>
		/// Network stream for writing to the client.
		/// </param>
		private void sendFile (String fileName, long fileSize, NetworkStream io)
		{
			var fs = new FileStream (fileName, FileMode.Open);

			//Buffer we use to send data
			var buff = new byte[BUFSIZE];

			//Bytes read from file
			int size = 0;

			while ((size = fs.Read(buff,0,BUFSIZE)) > 0) {
				io.Write (buff, 0, size);
			}
		}

		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name='args'>
		/// The command-line arguments.
		/// </param>
		public static void Main (string[] args)
		{
			Console.WriteLine ("Server starts...");
			new file_server ();
		}
	}
}
