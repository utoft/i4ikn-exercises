using System;
using System.Threading;

namespace Exercise9
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//Server init
			var server = new StatusDataServer (9001);

			var sThread = new Thread (server.run);
			sThread.Start ();

			Thread.Sleep (1000);

			//client init
			var ip = System.Net.IPAddress.Parse ("127.0.0.1");
			var port = 9001;
			var client = new StatusDataClient(new System.Net.IPEndPoint(ip,port));
				
			client.sendRequest ('U');
			client.sendRequest ('L');

			client.sendRequest ('u');
			client.sendRequest ('l');
		}
	}
}
