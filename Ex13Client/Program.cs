using System;
using System.Text;
using Ex13Lib;

namespace Ex13Lib.Client
{
	class MainClass
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name='args'>
		/// First argument: Filname
		/// </param>
		public static void Main (string[] args)
		{
			new slip_client (args);
		}
	}
}
