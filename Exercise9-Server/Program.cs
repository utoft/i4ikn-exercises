using System;

namespace Exercise9
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var port = 9001;
			var server = new StatusDataServer (port);

			Console.WriteLine ("Server: Listening on port {0}", port);

			server.run ();
		}
	}
}
