using System;
using System.Net;
using System.Net.Sockets;

namespace Exercise9
{
	public class StatusDataServer
	{
		int _port;
		IPEndPoint _endpoint;
		UdpClient listener;

		public StatusDataServer (int port)
		{
			_port = port;
			_endpoint = new IPEndPoint (IPAddress.Any, _port);
			listener = new UdpClient(_endpoint);
		}

		public void run()
		{
			while (true) {
				var requestData = SendRecieveLib.RecieveData (listener, _endpoint);
				string requestString = requestData.Item2;
				IPEndPoint clientEndpoint = requestData.Item1;
				string inputFile = string.Empty;

				//Write the request to console (for debugging)
				Console.WriteLine ("Server: Received broadcast from {0} :\n {1}\n", clientEndpoint, requestString);

				//Find the correct path which we need to read
				switch(requestString[0])
				{
				case 'U':
				case 'u':
					inputFile = "/proc/uptime";
					break;
				case 'L':
				case 'l':
					inputFile = "/proc/loadavg";
					break;
				default: //Anything else, ignore
					continue;
				}

				//Open and read a file
				using (var file = new System.IO.StreamReader(inputFile)) {
					string toReturn = file.ReadLine (); //Read text which we return
					SendRecieveLib.SendData (listener, clientEndpoint, toReturn); //Send data
				}
			}
		}
	}
}

