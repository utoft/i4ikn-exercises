using System;
using System.Net;
using System.Net.Sockets;

namespace Exercise9
{
	public class StatusDataClient
	{
		IPEndPoint _endpoint;
		UdpClient listener;

		public StatusDataClient (IPEndPoint server)
		{
			_endpoint = server;
			listener = new UdpClient ();
		}

		//Defaults to request uptime
		public void sendRequest(char requestInput = 'U')
		{
			SendRecieveLib.SendData (listener, _endpoint, requestInput.ToString());
			Console.WriteLine ("Client: Send request to {0} :\n {1}\n", _endpoint, requestInput);
			var response = SendRecieveLib.RecieveData (listener, _endpoint);

			Console.WriteLine ("Client: Received data from {0} :\n {1}\n", response.Item1, response.Item2);
		}
	}
}

