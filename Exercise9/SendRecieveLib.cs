using System;
using System.Net;
using System.Net.Sockets;

namespace Exercise9
{
	public static class SendRecieveLib
	{
		public static void SendData (UdpClient client, IPEndPoint reciever, string data)
		{
			//Before sending the data the text have to be encoded into a byte array
			//This is nessesary as we can not be sure that sender and reviecer pr default uses the same encoding
			byte[] toSend = System.Text.Encoding.UTF8.GetBytes (data); //encode using utf8

			//Send the packet
			client.Send (toSend, toSend.Length, reciever); //Send package
		}

		public static Tuple<IPEndPoint,string> RecieveData (UdpClient server, IPEndPoint localEndpoint)
		{
			//Endpoint of the client. A variable is required as the Recieve method uses the ref tag
			var clientEndpoint = new IPEndPoint (IPAddress.Any, localEndpoint.Port); //Will be overridden

			//Byte array recieved from sender. It is encoded in utf-8
			byte[] requestBytes = server.Receive (ref clientEndpoint); //Read(blocking) data request from client
			//Convert the byte array back to a string
			string request = System.Text.Encoding.UTF8.GetString (requestBytes); //convert to string

			//To return both the client-endpoint and the recieved string we wrap it in a tuple.
			return Tuple.Create (clientEndpoint, request);
		}
	}
}

