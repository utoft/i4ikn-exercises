using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace tcp
{
	public class file_client
	{
		/// <summary>
		/// The PORT.
		/// </summary>
		const int PORT = 9000;
		/// <summary>
		/// The BUFSIZE.
		/// </summary>
		const int BUFSIZE = 1000;

		IPAddress serverAddress;

		string fileToRecieve;

		/// <summary>
		/// Initializes a new instance of the <see cref="file_client"/> class.
		/// </summary>
		/// <param name='args'>
		/// The command-line arguments. First ip-adress of the server. Second the filename
		/// </param>
		public file_client (string[] args)
		{
			serverAddress = IPAddress.Parse (args [0]);
			Console.WriteLine ("Client: Server Address: {0}", serverAddress.ToString());
			fileToRecieve = args [1];
			Console.WriteLine ("Client: Server File: {0}", fileToRecieve);

			var endpoint = new IPEndPoint (serverAddress, PORT);
			TcpClient client = new TcpClient ();
			client.Connect (endpoint);
			var stream = client.GetStream ();
			this.receiveFile (fileToRecieve, stream);
		}

		/// <summary>
		/// Receives the file.
		/// </summary>
		/// <param name='fileName'>
		/// File name.
		/// </param>
		/// <param name='io'>
		/// Network stream for reading from the server
		/// </param>
		private void receiveFile (String fileName, NetworkStream io)
		{
			// TO DO Your own code
			LIB.writeTextTCP (io, fileName);
			var fileSize = LIB.getFileSizeTCP(io);
			var fileFromServer = LIB.extractFileName (fileName);

			if (fileSize == 0) {
				Console.WriteLine ("Client: File {0} not found on server", fileFromServer);
				return;
			}

			var fileContent = new FileStream (@"/root/Desktop/" + fileFromServer, FileMode.Create, FileAccess.ReadWrite);
			byte[] receivedPacket = new byte[BUFSIZE];

			int readBytes = 0; // Bytes in each package
			int totalBytesRead = 0; //total bytes recieved

			Console.WriteLine ("Client: Recieving file...");
			while (fileSize > totalBytesRead) //Continue until last packace is not BUFSIZE or the file size is reached
			{
				readBytes = io.Read (receivedPacket, 0, BUFSIZE); //Try to read BUFSIZE bytes from stream

				fileContent.Write(receivedPacket, 0, readBytes); //Write to file

				totalBytesRead += readBytes; //Sum total read bytes
			}

			fileContent.Close (); //close and flush file
			Console.WriteLine ("Client: Done recieving file");

		}

		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name='args'>
		/// The command-line arguments.
		/// </param>
		public static void Main (string[] args)
		{
			Console.WriteLine ("Client: starts...");
			new file_client(args);
		}
	}
}
