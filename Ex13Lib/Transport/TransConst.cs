using System;

namespace Ex13Lib
{
    public class TransportConstants
    {
        public static int SIZE_HEADER = 4;
        public static int SIZE_ACK = 4;
        public static int SIZE_CHKSUM = 2;

        public static int INDEX_CHKHIGH = 0;
        public static int INDEX_CHKLOW = 1;
        public static int INDEX_SEQ = 2;
        public static int INDEX_TYPE = 3;

        public static byte TYPE_ACK = 1;
        public static byte TYPE_DATA = 0;

    }
    public enum TransSize
    {
        CHKSUMSIZE = 2,
        ACKSIZE = 4
    };

    public enum TransCHKSUM
    {
        CHKSUMHIGH = 0,
        CHKSUMLOW = 1,
        SEQNO = 2,
        TYPE = 3
    };

    public enum TransType
    {
        DATA = 0,
        ACK = 1
    };
}

