using System;

namespace Ex13Lib
{
    public class Checksum
    {
        /// <summary>
        /// Calculate 16bit checksum from index and on size bytes
        /// </summary>
        /// <returns>
        /// The Checksum
        /// </returns>
        public long checksum(byte[] buf, int index, int size)
        {
            int i = index;
            long sum = 0;

            for (; size > 0; size--)
            {
                sum += (buf[i++] & 0xff) << 8;
                if ((--size) == 0) break;
                sum += (buf[i++] & 0xff);
            }

            return ((sum & 0xFFFF) + (sum >> 16)) & 0xFFFF;
        }

        public bool checkChecksum(byte[] buf, int index, int size)
        {
            var checkSum = checksum(buf, index, size - TransportConstants.SIZE_CHKSUM);
            var fromPacket = (buf[TransportConstants.INDEX_CHKHIGH] << 8 | buf[TransportConstants.INDEX_CHKLOW]);
            var chkResult = checkSum == fromPacket;
            return chkResult;
        }

        public bool checkChecksum(byte[] buf, int size)
        {
            return checkChecksum(buf, TransportConstants.SIZE_CHKSUM, size);
        }

        public void calcChecksum (ref byte[] buf, int size)
        {
            long sum = checksum(buf, TransportConstants.SIZE_CHKSUM, size - TransportConstants.SIZE_CHKSUM);

            buf.SetValue((byte)((sum >> 8) & 0xff), TransportConstants.INDEX_CHKHIGH);
            buf.SetValue((byte)(sum & 0xff), TransportConstants.INDEX_CHKLOW);
        }
    }
}

