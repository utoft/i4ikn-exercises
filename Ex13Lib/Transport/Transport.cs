using System;

/// <summary>
/// Transport.
/// </summary>
using System.Diagnostics;
using System.Timers;

namespace Ex13Lib
{
    /// <summary>
    /// Transport.
    /// </summary>
    public class Transport
    {
        /// <summary>
        /// The link.
        /// </summary>
        private ILink link;

        /// <summary>
        /// The 1' complements checksum.
        /// </summary>
        private Checksum checksum;

        /// <summary>
        /// The buffer.
        /// </summary>
        private byte[] buffer;

        /// <summary>
        /// The seq no. When sending
        /// </summary>
        private byte seqNo;

        /// <summary>
        /// The old_seq no. When Recieving
        /// </summary>
        private byte rec_seqNo;

        /// <summary>
        /// The error count.
        /// </summary>
        private int errorCount;

        /// <summary>
        /// The DEFAULT_SEQNO.
        /// </summary>
        private const int DEFAULT_SEQNO = 2;

        /// <summary>
        /// The DEFAULT_TIMEOUT.
        /// </summary>
        private const int DEFAULT_TIMEOUT = 1000; //ms

        /// <summary>
        /// Size of the package we are currently trying to send
        /// </summary>
        private int size_packet = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="Transport"/> class.
        /// </summary>
        public Transport(int BUFSIZE, ILink link = null)
        {
            this.link = link ?? new Link(BUFSIZE + TransportConstants.SIZE_HEADER);

            checksum = new Checksum();
            buffer = new byte[BUFSIZE + TransportConstants.SIZE_HEADER];
            seqNo = 0;
            rec_seqNo = 0;
            errorCount = 0;

//            timer = new Timer(DEFAULT_TIMEOUT);
//            timer.AutoReset = true;
//            timer.Elapsed += (object sender, ElapsedEventArgs e) => link.send(buffer, size_packet);
        }

        /// <summary>
        /// Receive ack and get success/failed
        /// </summary>
        /// <returns>
        /// True if ok/success
        /// </returns>
        private bool receiveAck()
        {
            var buf = new byte[TransportConstants.SIZE_ACK];
            int size = link.receive(ref buf);

            //if (size != (int)TransSize.ACKSIZE) return false;

            var checkSuccess = checksum.checkChecksum(buf, TransportConstants.SIZE_ACK);
            if (!checkSuccess ||
                    buf[TransportConstants.INDEX_SEQ] == seqNo ||
                    buf[TransportConstants.INDEX_TYPE] != TransportConstants.TYPE_ACK)
                return false;

//            seqNo = buf[TransportConstants.INDEX_SEQ];

            return true;
        }

        /// <summary>
        /// Sends the ack.
        /// </summary>
        /// <param name='ackNext'>
        /// Ack type.
        /// </param>
        private void sendAck(bool ackNext)
        {
            var ackBuf = new byte[TransportConstants.SIZE_ACK];

            var seqToAck = buffer[TransportConstants.INDEX_SEQ]; //Return if not ok

            if (ackNext) //Increment seq if ok
                seqToAck = NextSeqNo(seqToAck);

            Pack(ref ackBuf, seqToAck, TransportConstants.TYPE_ACK, TransportConstants.SIZE_ACK);

            link.send(ackBuf, TransportConstants.SIZE_ACK);
        }

        /// <summary>
        /// Send the specified buffer and size.
        /// </summary>
        /// <param name='buffer'>
        /// Buffer.
        /// </param>
        /// <param name='size'>
        /// Size.
        /// </param>
        public void send(byte[] buf, int size)
        {
            size_packet = size + 4;
            Array.Copy(buf, 0, buffer, 4, size);
            seqNo = NextSeqNo(seqNo);

            Pack(ref buffer, seqNo, TransportConstants.TYPE_DATA, size_packet);

            for (;;)
            {
                link.send(buffer, size_packet);

                var ackResult = receiveAck();

                if (ackResult)
                {
                    Debug.WriteLine("Sender: Ok");
                    break;
                }
                else
                {
                    Debug.WriteLine("Sender: Not OK");
                    
                }
            }
        }

        /// <summary>
        /// Receive the specified buffer.
        /// </summary>
        /// <param name='buffer'>
        /// Buffer.
        /// </param>
        public int receive(ref byte[] buf)
        {
            var size = 0;
            bool checkSumFalse = true;
            do
            {
                size = link.receive(ref buffer);

                var checksumResult = checksum.checkChecksum(buffer, size);

                if (!checksumResult)
                {
                    Debug.WriteLine("Reciever: ChkSumFail");
                    sendAck(false); //failed
                    checkSumFalse = true;
                }
                else
                {
                    var seqRecieved = GetSequenceNumber(buffer);

                    sendAck(true); //success

                    //What if same id recieved twise
                    if (rec_seqNo != seqRecieved)
                    {
                        Debug.WriteLine("Reciever: Ok");
                        rec_seqNo = seqRecieved;
                        checkSumFalse = false;
                    }
                    else
                    {
                        //Skip package
                        errorCount++;
                        Debug.WriteLine("Reciever: Dulicate");
                    }
                }
            } while (checkSumFalse);

            Array.Copy(buffer, 4, buf, 0, size - 4);
            return size - 4;
        }

        protected void Pack(ref byte[] data, byte seqNumber, byte type, int size)
        {
            //seq
            data[TransportConstants.INDEX_SEQ] = seqNumber;

            //type
            data[TransportConstants.INDEX_TYPE] = type;

            //Checksum
            checksum.calcChecksum(ref data, size);
        }

        public byte NextSeqNo(byte seqNo)
        {
            return (byte)((seqNo + 1) % 2);
        }

        public byte GetSequenceNumber(byte[] package)
        {
            return package[TransportConstants.INDEX_SEQ];
        }
    }
}