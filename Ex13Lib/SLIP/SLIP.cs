﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ex13Lib
{
    public class SLIP
    {
        const byte A = 65;
        const byte B = 66;
        const byte C = 67;
        const byte D = 68;

        public int Encode(byte[] source, int srcOffset, ref byte[] destination, int dstOffset, int count)
        {
            //Make temporary array with double the lenght of the source (nessesary if content is pure A or B's)
            var tempPos = dstOffset;

            for (var i = srcOffset; i < count + srcOffset; i++)
            {
                switch (source[i])
                {
                    case A:
                        destination[tempPos++] = B;
                        destination[tempPos++] = C;
                        break;
                    case B:
                        destination[tempPos++] = B;
                        destination[tempPos++] = D;
                        break;
                    default:
                        destination[tempPos++] = source[i];
                        break;
                }
            }

            return tempPos;
        }

        public int Decode(byte[] source, int srcOffset, ref byte[] destination, int dstOffset, int count)
        {
            var tempPos = dstOffset;

            for (var i = srcOffset; i < srcOffset + count; i++)
            {
                if (source[i] == B)
                    switch (source[++i])
                    {
                        case C:
                            destination[tempPos++] = A;
                            break;
                        case D:
                            destination[tempPos++] = B;
                            break;
                        default:
                            destination[tempPos++] = source[i];
                            break;
                    }
                else
                {
                    destination[tempPos++] = source[i];
                }
            }

            return tempPos;
        }
    }
}
