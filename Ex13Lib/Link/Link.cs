using System;
using System.IO.Ports;

/// <summary>
/// Link.
/// </summary>
namespace Ex13Lib
{
	/// <summary>
	/// Link.
	/// </summary>
	public class Link : ILink
	{
		/// <summary>
		/// The DELIMITE for slip protocol.
		/// </summary>
		const byte DELIMITER = (byte)'A';
		/// <summary>
		/// The buffer for link.
		/// </summary>
		private byte[] buffer;
		/// <summary>
		/// The serial port.
		/// </summary>
		SerialPort serialPort;

		/// <summary>
		/// Initializes a new instance of the <see cref="link"/> class.
		/// </summary>
		public Link (int BUFSIZE)
		{
			// Create a new SerialPort object with default settings.
			serialPort = new SerialPort("/dev/ttyS1",115200,Parity.None,8,StopBits.One);

			if(!serialPort.IsOpen)
				serialPort.Open();

			buffer = new byte[(BUFSIZE*2)+2];
		}

		/// <summary>
		/// Send the specified buf and size.
		/// </summary>
		/// <param name='buf'>
		/// Buffer.
		/// </param>
		/// <param name='size'>
		/// Size.
		/// </param>
		public void send (byte[] buf, int size)
		{
	    	// TO DO Your own code
			var slip = new SLIP ();
			var count = slip.Encode (buf, 0, ref buffer, 1, size);

			buffer [0] = DELIMITER; //Put 'A'
			buffer [count] = DELIMITER;
			serialPort.Write (buffer, 0, count+1);
		}

		/// <summary>
		/// Receive the specified buf and size.
		/// </summary>
		/// <param name='buf'>
		/// Buffer.
		/// </param>
		/// <param name='size'>
		/// Size. or -1 if an error occured
		/// </param>
		public int receive (ref byte[] buf)
		{
	    	// TO DO Your own code
			var beginRead = false;
			int pos = 0;

			int readByte;

			while (!beginRead) 
			{
				readByte = serialPort.ReadByte ();
				if (readByte == DELIMITER) 
				{
					beginRead = true;
				}
			}

			readByte = 0;

			while (readByte != DELIMITER) 
			{
				readByte = serialPort.ReadByte ();
				if (readByte != DELIMITER) {
					buffer [pos++] = (byte)readByte;
				}
			}

			var slip = new SLIP ();

			return slip.Decode (buffer, 0, ref buf, 0, pos);
		}
	}
}
