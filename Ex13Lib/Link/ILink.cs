﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ex13Lib
{
    public interface ILink
    {
        void send(byte[] buf, int size);
        int receive(ref byte[] buf);
    }
}
