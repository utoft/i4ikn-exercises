using System;
using System.IO;
using System.Text;

namespace Ex13Lib
{
	public class slip_client
	{
		/// <summary>
		/// The BUFSIZE.
		/// </summary>
		const int BUFSIZE = 1000;

		/// <summary>
		/// Initializes a new instance of the <see cref="slip_client"/> class.
		/// 
		/// slip_client metoden opretter en peer-to-peer forbindelse
		/// Sender en forspÃ¸rgsel for en bestemt fil om denne findes pÃ¥ serveren
		/// Modtager filen hvis denne findes eller en besked om at den ikke findes (jvf. protokol beskrivelse)
		/// Lukker alle streams og den modtagede fil
		/// Udskriver en fejl-meddelelse hvis ikke antal argumenter er rigtige
		/// </summary>
		/// <param name='args'>
		/// Filnavn med evtuelle sti.
		/// </param>
	    public slip_client(String[] args)
	    {
			var fileToRecieve = args [0];
			var transport = new Transport (BUFSIZE);
			Console.WriteLine ("Client: Server File: {0}", fileToRecieve);

			this.receiveFile (fileToRecieve, transport);
	    }

		/// <summary>
		/// Receives the file.
		/// </summary>
		/// <param name='fileName'>
		/// File name.
		/// </param>
		/// <param name='transport'>
		/// Transportlaget
		/// </param>
		private void receiveFile (String fileName, Transport transport)
		{
			byte[] buffer;

			buffer = Encoding.UTF8.GetBytes (fileName);

			//Request file
			transport.send (buffer,buffer.Length);

			buffer = new byte[BUFSIZE];

			//Recieve length
			transport.receive (ref buffer);

			var fileSize = BitConverter.ToInt64(buffer, 0);

			var saveLocation = LIB.extractFileName (fileName);

			if (fileSize == 0) {
				Console.WriteLine ("Client: File {0} not found on server", saveLocation);
				return;
			}

			var fileContent = new FileStream (@"/root/Desktop/" + saveLocation, FileMode.Create, FileAccess.ReadWrite);

			int readBytes = 0; // Bytes in each package
			int totalBytesRead = 0; //total bytes recieved

			Console.WriteLine ("Client: Recieving file...");
			while (fileSize > totalBytesRead) //Continue until last packace is not BUFSIZE or the file size is reached
			{
				readBytes = transport.receive(ref buffer); //Try to read bytes from stream

				fileContent.Write(buffer, 0, readBytes); //Write to file

				totalBytesRead += readBytes; //Sum total read bytes
			}

			fileContent.Close (); //close and flush file
			Console.WriteLine ("Client: Done recieving file");
		}
	}
}
