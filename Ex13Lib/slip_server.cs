using System;
using System.IO;
using System.Text;

namespace Ex13Lib
{
	public class slip_server
	{
		/// <summary>
		/// The BUFSIZE
		/// </summary>
		private const int BUFSIZE = 1000;

		/// <summary>
		/// Initializes a new instance of the <see cref="slip_server"/> class.
		/// </summary>
		public slip_server ()
		{
			var buffer = new byte[BUFSIZE];
			Transport t = new Transport (BUFSIZE);
			Console.WriteLine ("Server: Listening for connections");

			var filenameLength = t.receive (ref buffer);

			Console.WriteLine ("Server: Recieved connection");

			var fileName = Encoding.UTF8.GetString (buffer, 0, filenameLength);

			Console.WriteLine ("Server: Requested file is {0}", fileName);


			Int64 fileSize = LIB.check_File_Exists (fileName); //0 if not found

			//Send file size
			var fileSizeAsByte = BitConverter.GetBytes (fileSize);
			t.send (fileSizeAsByte, fileSizeAsByte.Length);

			Console.WriteLine ("Server: Sending file size of {0} bytes", fileSize);

			if (fileSize > 0) {
				Console.WriteLine ("Server: Sending file...");
				this.sendFile (fileName, fileSize, t); //Send file
				Console.WriteLine ("Server: Sending file done");
			}
			//}

			Console.WriteLine ("Server: Shutting down. Bye :)");
		}

		/// <summary>
		/// Sends the file.
		/// </summary>
		/// <param name='fileName'>
		/// File name.
		/// </param>
		/// <param name='fileSize'>
		/// File size.
		/// </param>
		/// <param name='tl'>
		/// Tl.
		/// </param>
		private void sendFile(String fileName, long fileSize, Transport transport)
		{
			var fs = new FileStream (fileName, FileMode.Open);

			//Buffer we use to send data
			var buff = new byte[BUFSIZE];

			//Bytes read from file
			int size = 0;

			while ((size = fs.Read(buff,0,BUFSIZE)) > 0) {
				transport.send(buff, size);
			}
		}
	}
}
