Server output:
root@machine1:~# ./file_server.exe 
Server starts...
Server: Listening for connections on 0.0.0.0:9000...
Server: Recieved connection from 192.168.183.130:49003
Server: Requested file is ../../../boo.jpg
Server: Sending file size of 208072 bytes
Server: Sending file...
Server: Sending file done
Server: Shutting down. Bye :)
root@machine1:~#

Client output:
root@machine2:~# ./file_client.exe 192.168.183.129 ../../../boo.jpg
Client: starts...
Client: Server Address: 192.168.183.129
Client: Server File: ../../../boo.jpg
Client: Recieving file...
Client: Done recieving file
root@machine2:~#

Check that contents match:
## Server side ##
root@machine1:~/Documents/Exercise 8# md5sum boo.jpg 
a3e57e69d54022fc71ecf6de2d476282  boo.jpg

## Client side ##
root@machine2:~/Desktop# md5sum boo.jpg 
a3e57e69d54022fc71ecf6de2d476282  boo.jpg