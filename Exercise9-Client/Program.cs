using System;

namespace Exercise9
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var ip = System.Net.IPAddress.Parse (args [0]);
			var port = int.Parse (args [1]);
			var client = new StatusDataClient(new System.Net.IPEndPoint(ip,port));
			if (args.Length >= 3)
				client.sendRequest (args [2][0]);
			else
				client.sendRequest ();
		}
	}
}
