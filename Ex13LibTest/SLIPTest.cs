﻿using Ex13Lib;
using NUnit.Framework;
using System.Text;

namespace Ex13LibTest
{
    [TestFixture]
    public class SLIPTest
    {
        private SLIP _u;

        [SetUp]
        public void SetUp()
        {
            _u = new SLIP();
        }

        public string ToStr(byte[] bytes, int length)
        {
            return Encoding.ASCII.GetString(bytes, 0, length);
        }

        public byte[] ToByte(string text)
        {
            return Encoding.ASCII.GetBytes(text);
        }

        [Test, Sequential]
        public void Encode_ALL([Values("", "A", "AA", "B", "BB", "ABCD")] string input, [Values("", "BC", "BCBC", "BD", "BDBD", "BCBDCD")] string output)
        {
            var test = ToByte(input);
            var destBuf = new byte[test.Length * 2];

            var length = _u.Encode(test, 0, ref destBuf, 0, test.Length);

            Assert.That(ToStr(destBuf, length), Is.EqualTo(output));
        }

        [Test, Sequential]
        public void Decode_ALL([Values("", "BC", "BCBC", "BD", "BDBD", "BCBDCD")] string input, [Values("", "A", "AA", "B", "BB", "ABCD")] string output)
        {
            var test = ToByte(input);
            var destBuf = new byte[test.Length * 2];

            var length = _u.Decode(test, 0, ref destBuf, 0, test.Length);

            Assert.That(ToStr(destBuf, length), Is.EqualTo(output));
        }
    }
}