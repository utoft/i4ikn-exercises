using System;
using Ex13Lib;
using System.Collections.Concurrent;

namespace Ex13LibTest.Mocks
{
    public class SharedLink
    {
        public delegate void InOutAction(string name, SubLink.InOut inOut, Tuple<byte[], int> obj, BlockingCollection<Tuple<byte[], int>> queue);
        //Action<string, SubLink.InOut, Tuple<byte[], int>, BlockingCollection<Tuple<byte[], int>>>

        public ILink Server { get; set; }

        public ILink Client { get; set; }

        //public byte[] toServer;
        //public byte[] toClient;

        public BlockingCollection< Tuple<byte[],int> > qServer;
        public BlockingCollection< Tuple<byte[],int> > qClient;

        //public int toServerSize;
        //public int toClientSize;

        public int size; 

        public SharedLink (int size)
        {
            qServer = new BlockingCollection< Tuple<byte[], int> >(10);
            qClient = new BlockingCollection< Tuple<byte[],int> > (10);

            this.size = size;

            Server = new SubLink (this, true);
            Client = new SubLink (this, false);
        }

        public SharedLink(int size, InOutAction onPackage)
        {
            qServer = new BlockingCollection<Tuple<byte[], int>>(10);
            qClient = new BlockingCollection<Tuple<byte[], int>>(10);

            //toServer = new byte[size];
            //toClient = new byte[size];
            this.size = size;

            //toServerSize = 0;
            //toClientSize = 0;

            Server = new SubLink(this, true, onPackage);
            Client = new SubLink(this, false, onPackage);
        }
    }
}

