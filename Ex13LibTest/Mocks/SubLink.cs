﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Ex13Lib;

namespace Ex13LibTest.Mocks
{
    public class SubLink : ILink
    {
        private SharedLink _master;
        private bool _isServer;
        private SharedLink.InOutAction onAction;

        public string Name
        {
            get { return _isServer ? "Server" : "Client"; }
        }

        public BlockingCollection<Tuple<byte[], int>> MyQueue
        {
            get { return _isServer ? _master.qServer : _master.qClient; }
        }

        public BlockingCollection<Tuple<byte[], int>> TheirQueue
        {
            get { return _isServer ? _master.qClient : _master.qServer; }
        }

        public enum InOut
        {
            Recieve,
            Send
        }

        public SubLink(SharedLink master, bool isServer)
        {
            _master = master;
            _isServer = isServer;
            onAction = null;
        }

        public SubLink(SharedLink master, bool isServer, SharedLink.InOutAction handlePackage)
        {
            _master = master;
            _isServer = isServer;
            onAction = handlePackage;
        }

        public void send(byte[] buf, int size)
        {
            var copy = new byte[size];
            Array.Copy(buf,copy,size);

            var toSave = new Tuple<byte[], int>(copy, size);
            if (onAction != null) onAction(Name, InOut.Send, toSave, TheirQueue);

            else
            {
                TheirQueue.Add(toSave);
            }
        }

        public int receive(ref byte[] buf)
        {
            Tuple<byte[], int> tempPackage = null;
            
                Debug.WriteLine("Link: Debug: {0}{1}: Recieve ...", Name,"");
                tempPackage = MyQueue.Take();

                Debug.WriteLine("Link: Debug: {0}: Recieved {1} byte", Name, tempPackage.Item2);

                if (onAction != null) onAction(Name, InOut.Recieve, tempPackage, null);

            Array.Copy(tempPackage.Item1, 0, buf, 0, tempPackage.Item2);

            return tempPackage.Item2;

        }
    }
}
