﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ex13Lib;
using Ex13LibTest.Mocks;
using NSubstitute;
using NUnit.Framework;

namespace Ex13LibTest
{
    [TestFixture]
    class TransportSeqTest
    {
        private Transport _c;
        private Transport _s;
        private SharedLink link;

        private const int BUFSIZE = 1000;

        [SetUp]
        public void SetUp()
        {
            SetUp(BUFSIZE, (name, inout, arg, queue) =>
            {
                Debug.WriteLine("Queue: {0}, {1}, {2}", name, inout, arg.Item2);

                if (inout == SubLink.InOut.Send)
                    queue.Add(arg);
            });
        }

        public void SetUp(int buffer, SharedLink.InOutAction action = null)
        {
            link = new SharedLink(buffer, action);

            _c = new Transport(BUFSIZE, link.Client);
            _s = new Transport(BUFSIZE, link.Server);
        }

        [Test]
        public void Send_Then_Recieve()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE));
            sendTask.Wait(100);

            _c.receive(ref cBuff);

            //Assert.That(recieveTask.Wait(500), Is.True, "SuccessFullRecieve");
        }

        [Test]
        public void Send1Recive1()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var recieveTask = Task.Factory.StartNew(() => _c.receive(ref cBuff));

            Assert.That(recieveTask.Wait(500), Is.False, "NothingRecieved");
            
            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE));

            sendTask.Wait(); //Send is done

            Assert.That(recieveTask.Wait(500), Is.True, "SuccessFullRecieve");
        }

        [Test]
        public void Send1Recive1_CorrectLength()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE/2));

            //recieveTask.Wait();

            var res = _c.receive(ref cBuff);
            Assert.That(res, Is.EqualTo(BUFSIZE / 2));
        }

        [Test]
        public void Send1Recive1_SenderGetAck()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE / 2));

            var res = _c.receive(ref cBuff);

            Assert.That(res, Is.EqualTo(BUFSIZE / 2));
        }

        [Test]
        public void Send2Recive2()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() =>
            {
                _s.send(sBuff, BUFSIZE/2);
                _s.send(sBuff, BUFSIZE/2);
            });

            var recieveTask = Task.Factory.StartNew(() =>
            {
                var res1 = _c.receive(ref cBuff);
                var res2 = _c.receive(ref cBuff);
            });

            Assert.That(recieveTask.Wait(100), Is.True);
        }

        [Test]
        public void PingPong2()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() =>
            {
                _c.receive(ref sBuff);
                _c.send(sBuff, BUFSIZE / 2);
                _c.receive(ref sBuff);
                _c.send(sBuff, BUFSIZE / 2);
            });

            var recieveTask = Task.Factory.StartNew(() =>
            {
                _s.send(sBuff, BUFSIZE / 2);
                _s.receive(ref sBuff);
                _s.send(sBuff, BUFSIZE / 2);
                _s.receive(ref sBuff);
            });

            Assert.That(Task.WaitAll(new Task[]{sendTask,recieveTask}, 100), Is.True);
        }

        [Test]
        public void Send1Recive1_Scrambled()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE / 2));

            var packet = link.qClient.Take();
            packet.Item1[0] += 1;
            link.qClient.Add(packet);

            var res = _c.receive(ref cBuff);

            Assert.That(res, Is.EqualTo(BUFSIZE / 2));
        }

        [Test]
        public void Send2Recive2_ScrambledACK()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];
            bool first = true;

            SetUp(BUFSIZE, (name, inout, arg, queue) =>
            {
                Debug.WriteLine("Queue: {0}, {1}, {2}", name, inout, arg.Item2);

                

                if (inout == SubLink.InOut.Send)
                {
                    if (first && arg.Item2.Equals(4) && name.Equals("Client")) //First package will be send twise
                    {
                        Debug.WriteLine("Queue: {0}, {1}, {2}, (Scrambled ack)", name, inout, arg.Item2);
                        arg.Item1[0] += 1; //Make checksum wrong
                        first = false;
                    }

                    queue.Add(arg);
                }
            });
            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE / 2));

            var res = _c.receive(ref cBuff);

            var sendTask2 = sendTask.ContinueWith((task1) =>
            {
                Debug.WriteLine("Server2: Sending");
                _s.send(sBuff, BUFSIZE/2);
            });

            var res2 = _c.receive(ref cBuff);

            Assert.That(sendTask2.Wait(100), Is.True);
        }

        [Test]
        public void Send2Recive2_Scrambled()
        {
            var sBuff = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE / 2));

            var packet = link.qClient.Take();
            packet.Item1[0] += 1;
            link.qClient.Add(packet);

            var res = _c.receive(ref cBuff);

            var sendTask2 = Task.Factory.StartNew(() => _s.send(sBuff, BUFSIZE / 2));

            var packet2 = link.qClient.Take();
            packet2.Item1[0] += 1;
            link.qClient.Add(packet2);

            var res2 = _c.receive(ref cBuff);

            Assert.That(res2, Is.EqualTo(BUFSIZE / 2));
        }

        [Test]
        public void Send3Recive2_Duplicate()
        {
            var sBuff1 = new byte[BUFSIZE];
            var sBuff2 = new byte[BUFSIZE];
            var cBuff = new byte[BUFSIZE];

            bool first = true;

            //Intercept send/recieve of packages
            SetUp(BUFSIZE, (name, inout, arg, queue) =>
            {
                Debug.WriteLine("Queue: {0}, {1}, {2}", name, inout, arg.Item2);

                if (first && inout == SubLink.InOut.Send && name.Equals("Server")) //First package will be send twise
                {
                    Debug.WriteLine("Queue: {0}, {1}, {2}, (Duplicate Injected)", name, inout, arg.Item2);
                    queue.Add(arg);
                    first = false;
                }

                if(inout == SubLink.InOut.Send)
                    queue.Add(arg);
            });

            sBuff1[0] = 1;
            sBuff2[0] = 2;

            var sendTask = Task.Factory.StartNew(() => _s.send(sBuff1, BUFSIZE / 2));

            var res = _c.receive(ref cBuff);

            var sendTask2 = Task.Factory.StartNew(() => _s.send(sBuff2, BUFSIZE / 2));

            var res2 = _c.receive(ref cBuff);

            Assert.That(cBuff[0], Is.EqualTo(2));
        }
    }
}
